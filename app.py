from flask import Flask, jsonify, request
from flask_restful import Resource, Api
import psycopg2
app = Flask(__name__)
api = Api(app)

bd='******'
usr='******'
serv='******'
psw= '******’

class AllItems(Resource):
    """Consult products or stores"""
    def get(self,table_consult):
        select_all = {
            'tiendas':"SELECT * FROM stores",
            'productos':"SELECT sku, name, category, brand FROM products",
        }
        conn = psycopg2.connect(dbname=bd, user=usr, host=serv, password=psw)
        cur = conn.cursor()
        cur.execute(select_all[table_consult])
        records = cur.fetchall()
        cur.close()
        conn.close()
        return jsonify({"Respuesta":records})


class Stock(Resource):
    """Consult the stock of a product"""
    def get(self,product):
        conn = psycopg2.connect(dbname=bd, user=usr, host=serv, password=psw)
        cur = conn.cursor()
        cur.execute("""SELECT SUM(quantity) FROM stock WHERE sku = %s""", [product])
        records = cur.fetchall()
        cur.close()
        conn.close()
        return jsonify({"Respuesta":records})


class EnoughProduct(Resource):
    """Consult the stock of a product in one store"""
    def get(self, store, product):
        return jsonify({"Respuesta":get_stock_store(store, product)})


class Sale(Resource):
    """Update stock if it is enough for the sale"""
    def put(self):
        if not request.json:
            return jsonify({"Respuesta":"La solicitud no se puede procesar."})
        if  not 'tienda':
            return jsonify({"Respuesta":"La solicitud no se puede procesar sin el identificador de la tienda."})
        if  not 'producto':
            return jsonify({"Respuesta":"La solicitud no se puede procesar sin el sku del producto."})
        if  not 'cantidad':
            return jsonify({"Respuesta":"La solicitud no se puede procesar sin la cantidad del producto."})
        product_json=request.json
        store = product_json['tienda']
        product = product_json['producto']
        quantity = product_json['cantidad']
        actual_stock=get_stock_store(store, product)
        if actual_stock > quantity:
            try:
                conn = psycopg2.connect(dbname=bd, user=usr, host=serv, password=psw)
                cur = conn.cursor()
                stock_up=actual_stock-quantity
                sql="""UPDATE stock	SET quantity = %s WHERE sku = %s AND store_id = %s """
                cur.execute(sql, (stock_up, product, store))
                conn.commit()
                cur.close()
                conn.close()
                return jsonify({"Respuesta":"Existencias de venta actualizadas."})
            except(Exception, psycopg2.Error) as error:
                print("Error connecting to PostgreSQL database", error)
                conn = None
                return jsonify({"Respuesta":"No se pudo realizar la operación."}) 
        else:
            return jsonify({"Respuesta":"Producto insuficiente en esta tienda."}) 


class Purchase(Resource):
    """Update the stock of a product later of a purchase """
    def put(self):
        if not request.json:
            return jsonify({"Respuesta":"La solicitud no se puede procesar."})
        if  not 'tienda':
            return jsonify({"Respuesta":"La solicitud no se puede procesar sin el identificador de la tienda."})
        if  not 'producto':
            return jsonify({"Respuesta":"La solicitud no se puede procesar sin el sku del producto."})
        if  not 'cantidad':
            return jsonify({"Respuesta":"La solicitud no se puede procesar sin la cantidad del producto."})
        product_json=request.json
        store = product_json['tienda']
        product = product_json['producto']
        quantity = product_json['cantidad']
        actual_stock=get_stock_store(store, product)
        stock_up=actual_stock+quantity
        try:
            
            conn = psycopg2.connect(dbname=bd, user=usr, host=serv, password=psw)
            cur = conn.cursor()
            sql="""UPDATE stock	SET quantity = %s WHERE sku = %s AND store_id = %s """
            cur.execute(sql, (stock_up, product, store))
            conn.commit()
            cur.close()
            conn.close()
            return jsonify({"Respuesta":"Existencias de compra actualizadas."})
        except(Exception, psycopg2.Error) as error:
            print("Error connecting to PostgreSQL database", error)
            conn = None
            return jsonify({"Respuesta":"No se pudo realizar la operación."}) 


class Product(Resource):
    """Insert new product"""
    def post(self):
        if not request.json:
            return jsonify({"Respuesta":"La solicitud no se puede procesar."})
        if  not 'sku':
            return jsonify({"Respuesta":"La solicitud no se puede procesar sin el sku del producto."})
        if  not 'nombre':
            return jsonify({"Respuesta":"La solicitud no se puede procesar sin el nombre del producto."})
        try:
            product_json=request.json
            id_product = product_json['sku']
            conn = psycopg2.connect(dbname=bd, user=usr, host=serv, password=psw)
            cur = conn.cursor()
            sql="""INSERT INTO products(sku, name, category, brand, list_price, description) VALUES (%s, %s, %s, %s, %s, %s) """
            cur.execute(sql, (id_product,
            product_json['nombre'],
            product_json.get('categoria',""),
            product_json.get('marca',""),
            product_json.get('precio',0.0),
            product_json.get('detalle',"")))
            conn.commit()
            stores = get_id_stores()
            for id_store in stores:
                sql1="""INSERT INTO stock(quantity, store_id, sku)	VALUES (%s, %s, %s) """
                cur.execute(sql1, (0, id_store, id_product))
                conn.commit()
            cur.close()
            conn.close()
            return jsonify({"Respuesta":"Producto agregado."})
        except(Exception, psycopg2.Error) as error:
            print("Error connecting to PostgreSQL database", error)
            conn = None
            return jsonify({"Respuesta":"No se pudo realizar la operación."}) 


def get_stock_store(store, product):
    """Get stock of a product in one store"""
    conn = psycopg2.connect(dbname=bd, user=usr, host=serv, password=psw)
    cur = conn.cursor()
    cur.execute("""SELECT quantity FROM stock WHERE sku = %s AND store_id = %s""", (product, store))
    records = cur.fetchone()
    cur.close()
    conn.close()
    return records[0] 

def get_id_stores():
    """Get id from all the stores"""
    conn = psycopg2.connect(dbname=bd, user=usr, host=serv, password=psw)
    cur = conn.cursor()
    cur.execute("""SELECT store_id FROM stores""")
    records = cur.fetchall()
    cur.close()
    conn.close()
    return records    


api.add_resource(AllItems, '/API/v1/<string:table_consult>')
api.add_resource(Stock, '/API/v1/existencias/<string:product>')
api.add_resource(EnoughProduct, '/API/v1/existencias/<int:store>/<string:product>')
api.add_resource(Sale, '/API/v1/venta')
api.add_resource(Purchase, '/API/v1/compra')
api.add_resource(Product, '/API/v1/nuevoproducto')


if __name__ == '__main__':
    app.run(debug=True)