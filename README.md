# Flask
Desarrollar una API Rest que implementa un sistema de inventario de productos.

## Instalación
Requerimientos:
- Entorno virtual (venv)
- Python 3.7, pip

### Entorno de Desarrollo

Este entorno permite realizar modificaciones en la API. 
Para ello es necesario crear un entorno virtual, donde se resguardan todas las paqueterías necesarias para desarrollar 
en la plataforma:

1. Crear entorno virtual (_venv_)
```shell script 
virtualenv venv
```
2. Activar entorno virtual
```shell script
source venv/bin/activate
```
3. Instalar paqueterías
```shell script
(venv) pip install -r flask/requirements.txt
```

Con estos pasos generamos el acceso a las bibliotecas necesarias para el desarrollo del proyecto.

1. Es necesario crear la base de dato necesaria en Postgres y agregar sus credenciales.
2. Para iniciar el servicio, ejecute el siguiente comando en la terminal dentro de la misma carpeta:
```shell script
python app.py
```
3. Ingresar a la dirección: http://127.0.0.1:5000/productos

Si todo se encuentra en orden, podrá observar la respuesta de la API.

4. Para detener el servicio, presione: CTRL+C


**Endpoints disponibles:**

- Consultar productos
GET: http://127.0.0.1:5000/API/v1/productos

- Consultar tiendas
GET: http://127.0.0.1:5000/API/v1/tiendas

- Consultar stock de un producto
GET: http://127.0.0.1:5000/API/v1/existencias/sku

- Consultar stock de un producto en una tienda
GET: http://127.0.0.1:5000/API/v1/existencias/tienda/sku

- Actualizar stock si es suficiente para la venta
PUT: http://127.0.0.1:5000/API/v1/venta

- Actualizar el stock de un producto después de una compra
PUT: http://127.0.0.1:5000/API/v1/compra

- Insertar nuevo producto
POST: http://127.0.0.1:5000/API/v1/nuevoproducto

